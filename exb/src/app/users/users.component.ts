import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userdata: any
  ngOnInit() {
    const fetchPromise = fetch('https://jsonplaceholder.typicode.com/todos');
    fetchPromise.then(response => {
      return response.json();
    }).then(user => {
      this.userdata = user;
    })
  }

  getUserColumnNames() {
    if (this.userdata !== undefined) {
      return Object.keys(this.userdata[0])
    }
  }

  getUserData() {
    if (this.userdata !== undefined) {
      let usersData = []
      for (let users of this.userdata) {
        usersData.push(users)
      }
      return usersData
    }
  }
}
